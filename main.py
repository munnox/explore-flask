import requests
from flask import Flask, redirect, request, session, url_for
from flask_oauthlib.client import OAuth

app = Flask(__name__)
oauth = OAuth(app)
app.secret_key = '<secret_key>'
app.debug = True

# Replace the client ID and secret below with the values obtained from Azure AD
azuread = oauth.remote_app(
    'azuread',
    consumer_key='<client_id>',
    consumer_secret='<client_secret>',
    request_token_params={
        'scope': 'openid profile email',
        'response_type': 'code'
    },
    base_url='https://login.microsoftonline.com/common/oauth2/v2.0/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://login.microsoftonline.com/common/oauth2/v2.0/token',
    authorize_url='https://login.microsoftonline.com/common/oauth2/v2.0/authorize'
)

@app.route('/test')
def test():
    return """<html><head><title>Flask Auth Test</title></head><body>Flask Auth Test</body></html>"""

@app.route('/')
def index():
    if 'azuread_token' in session:
        return redirect('/protected')
    return redirect(url_for('login'))

@app.route('/login')
def login():
    return azuread.authorize(callback=url_for('authorized', _external=True))
    # redirect_uri = url_for('authorized', _external=True)
    # return azuread.authorize_redirect(redirect_uri)

@app.route('/logout')
def logout():
    session.pop('azuread_token', None)
    return redirect('/')

@app.route('/authorized')
def authorized():
    print("authed----------------<<<<<<")
    resp = azuread.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    session['azuread_token'] = (resp['access_token'], '')
    return redirect('/protected')

@azuread.tokengetter
def get_azuread_oauth_token():
    return session.get('azuread_token')

@app.route('/protected', defaults={'path': ''})
@app.route('/protected/<path:path>')
def protected(path):
    if 'azuread_token' not in session:
        return redirect(url_for('login'))
    access_token = session['azuread_token'][0]
    headers = {'Authorization': 'Bearer ' + access_token}
    target_url = 'https://<target_website>/' + path
    resp = requests.request(
        method=request.method,
        url=target_url,
        headers=headers,
        data=request.data,
        params=request.args,
        allow_redirects=False
    )
    headers = [(name, value) for (name, value) in resp.headers.items()
               if name.lower() != 'transfer-encoding']
    response = app.make_response((resp.content, resp.status_code, headers))
    return response

if __name__ == '__main__':
    app.run(ssl_context="adhoc")