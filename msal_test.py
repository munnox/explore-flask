import msal
import json
import sys
import requests

# Azure Client GUID
client = "add_client_guid"
# Azure Tenant GUID
tenant = "add_tenant_guid"

config = {
    # "authority": f"https://login.microsoftonline.com/common",
    "authority": f"https://login.microsoftonline.com/{tenant}",
    "client_id": client,
    "scope": ["User.ReadBasic.All"],
    # // You can find the other permission names from this document
    # // https://docs.microsoft.com/en-us/graph/permissions-reference
    "endpoint": "https://graph.microsoft.com/v1.0/users"
    # // You can find more Microsoft Graph API endpoints from Graph Explorer
    # // https://developer.microsoft.com/en-us/graph/graph-explorer
}

from msal import PublicClientApplication

app = PublicClientApplication(
    config['client_id'],
    authority=config['authority'],
    # scopes=["User.Read"]
)

result = None  # It is just an initial value. Please follow instructions below.
# We now check the cache to see
# whether we already have some accounts that the end user already used to sign in before.
accounts = app.get_accounts()
if accounts:
    # If so, you could then somehow display these accounts and let end user choose
    print("Pick the account you want to use to proceed:")
    for a in accounts:
        print(a["username"])
    # Assuming the end user chose this one
    chosen = accounts[0]
    # Now let's try to find a token in cache for this account
    result = app.acquire_token_silent(config['scope'], account=chosen)

if not result:
    # So no suitable token exists in cache. Let's get a new one from AAD.
    # result = app.acquire_token_by_one_of_the_actual_method(..., scopes=["User.Read"])
    
    flow = app.initiate_device_flow(scopes=config["scope"])
    if "user_code" not in flow:
        raise ValueError(
            "Fail to create device flow. Err: %s" % json.dumps(flow, indent=4))

    print("Mesage:")
    print(flow["message"])
    sys.stdout.flush()  # Some terminal needs this to ensure the message is shown # Ideally you should wait here, in order to save some unnecessary polling
    # input("Press Enter after signing in from another device to proceed, CTRL+C to abort.")

    result = app.acquire_token_by_device_flow(flow)  # By default it will block
if "access_token" in result:
    print(result["access_token"])  # Yay!
else:
    print(result.get("error"))
    print(result.get("error_description"))
    print(result.get("correlation_id"))  # You may need this when reporting a bug

if "access_token" in result:
    # Calling graph using the access token
    graph_data = requests.get(  # Use token to call downstream service
        config["endpoint"],
        headers={'Authorization': 'Bearer ' + result['access_token']},).json()
    print("Graph API call result: %s" % json.dumps(graph_data, indent=2))
else:
    print(result.get("error"))
    print(result.get("error_description"))
    print(result.get("correlation_id"))  # You may need this when reporting a bug