{ pkgs ? import <nixpkgs> {} # here we import the nixpkgs package set
}:
let
  basepkgs = [
    pkgs.poetry
  ];
  scripthelpers = [];
in pkgs.mkShell {
  name="Development Environment";    # that requires a name
  buildInputs =  basepkgs ++ scripthelpers;

  shellHook = ''
    # bash to run when you enter the shell     
    echo "Start developing..."                                         
    poetry install
    export FLASK_APP=main:app
    poetry shell
  '';               
}